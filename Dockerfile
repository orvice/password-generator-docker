FROM golang as builder

RUN mkdir /app
WORKDIR /app

RUN git clone https://github.com/mrjooz/password-generator.git

FROM nginx

COPY --from=builder /app/password-generator/ /usr/share/nginx/html/